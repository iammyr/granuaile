Run:
 
```
#!bash

 $ virtualenv --system-site-packages ~/hackathon-virtualenv
 $ source ~/hackathon-virtualenv/bin/activate
 $ git clone git@bitbucket.org:iammyr/granuaile.git
 $ cd granuaile
 $ pip install flask
 $ python com/hackathon2016/granuaile/main.py
 $ export SLACK_API_TOKEN=xxxx
 (^^ask me for a valid token)
```